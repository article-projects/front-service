import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import MyComponent from './index';

describe('Unit test example', () => {
  test('Should show the welcome text', () => {
    const wrapper = shallow(
      <MyComponent />);
    expect(wrapper.text()).toEqual('Hi there!');
  });

  test('Should match with the snapshot', () => {
    const wrapper = shallow(
      <MyComponent />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
