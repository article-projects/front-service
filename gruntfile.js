module.exports = function (grunt) {
  grunt.initConfig({
    env: {
      ci: {
        NODE_ENV: 'continuous_integration',
        JEST_JUNIT_OUTPUT: './shippable/testresults/results.xml',
      },
    },
    run: {
      eslint: {
        exec: 'eslint .',
      },
      test: {
        exec: 'jest --config config/jest/jest.json',
      },
      test_and_update: {
        exec: 'jest --updateSnapshot --config config/jest/jest.json',
      },
    },
  });

  grunt.loadNpmTasks('grunt-env');
  grunt.loadNpmTasks('grunt-run');

  grunt.registerTask('default', [
    'env:ci',
    'run:test',
    'run:eslint',
  ]);

  grunt.registerTask('test_and_update', [
    'env:ci',
    'run:test_and_update',
    'run:eslint',
  ]);
};
